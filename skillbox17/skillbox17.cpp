﻿#include <iostream>
#include <math.h>

class Vector
{
public:
    Vector() 
    {}
    Vector(double _X, double _Y, double _Z) : x(_X),y(_Y),z(_Z)
    {}
    void Show()
    {
        std::cout <<"\n" << x <<" "<< y<<" "<< z << std::endl;
    }
    double length()
    { 
        return sqrt(x * x + y * y + z * z);
    }
private:
    double x;
    double y;
    double z;
};

int main()
{
    Vector v(3,2,4);
    v.Show();
    std::cout << v.length();

    
}